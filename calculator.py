class Calculator(object):

    """docstring for Calculator"""

    def __init__(self, *arg):
        super(Calculator, self).__init__()
        self.arg = arg

    def select(self):
        print("Selecione a operação: \n")
        print("Soma = 0\n")
        print("Subtracao = 1\n")
        print("Multiplicacao = 2\n")
        option = input("Divisao = 3\n")
        if option == "0":
            print(self.sum())
        elif option == "1":
            print(self.subtract())
        elif option == "2":
            print(self.multiply())
        elif option == "3":
            print(self.divide())

    def divide(self):
        dividendo = float(input('insira dividendo: '))
        divisor = float(input('insira divisor: '))
        quociente = dividendo / divisor
        return (quociente)

    def sum(self):
        print("Soma: \n ")
        primeiro_valor = input("Insira o primeiro valor ")
        segundo_valor = input("Insira o segundo valor: ")
        x = float(primeiro_valor)
        y = float(segundo_valor)
        soma_final = float(x + y)
        return soma_final

    def subtract(self):
        first_number = input("Insira primeiro numero: \n")
        second_number = input("Insira segundo numero: \n")
        x = float(first_number)
        y = float(second_number)
        result_subtract = float(x - y)
        return result_subtract

    def multiply(self):
        print("Multiplicacao: ")
        first = float(input("\nDigite o primeiro argumento: "))

        second = float(input("\nDigite o segundo argumento: "))
        return (first * second)

if __name__ == '__main__':
    cl = Calculator()
    cl.select()
